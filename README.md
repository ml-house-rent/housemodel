# HOUSE RENTALS MODEL
Library for building house rental linear models and perform predictions.
Currently, the only input feature for predictions are the square metres.

## Use the built-in model (recommended for production)
As user of the model, there's an already built model, consider the following snippet to perform predictions:
```python
# Get a linear model object for rental predictions
model = HouseRentModelBuilder().get_model()
# Load the built-in model
model.load_model()
# Preform predictions
m2_list = [90, 75, 80]
pred_list = model.predict(m2_list)
```

## Build your own model
You can use the `HouseRentModelBuilder` to build your custom linear model, it just needs a rentals dataset like the `RentalsDataset` already provided in `bcn_rental` module. To use it and make a new model:
```python
# Get a dataset and sintetize a few examples
rentals_dataset = RentalsDataset(50)
# Get the builder
model_builder = HouseRentModelBuilder(rentals_dataset)
# Train the model
model_builder.train()
# Get your baked model
model = model_builder.get_model()
# Perform predictions
m2_list = [90, 75, 80]
pred_list = model.predict(m2_list)
```

### (Optionally) Get built model info
The metrics obtainer are:
* MSE
* R2 score
* Learned parameters
```python
# (Optionally) Print the model metrics
model_builder.print_model_info()
# (Optionally) Plot the model over the test examples
model_builder.plot_model()
```

### (Optionally) Save the new model into the package
It will be saved into the `model.pkl` pickle file inside the package.
```python
model.save_model()
```
