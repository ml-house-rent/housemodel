# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.2'
#       jupytext_version: 1.2.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %%
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


# %%
class RentalsDataset(object):
    ''' Rentals dataset for model training '''
    
    # DATASET FILE
    DATASET_FILE = 'bcn_rental_dataset.csv'
    CSV_DELIMITER = ','
    
    def __init__(self, quantity=100):
        ''' Dataset constructor '''
        self.rentals = self._load_dataset()
        self._sintetize_new_data(quantity)
        # Use only the m2 feature and price as target
        m2_X = np.array([self.rentals['m2'].iloc[:].values]).T
        price_Y = np.array([self.rentals['price_per_month'].iloc[:].values]).T
        test_remain = int(price_Y.shape[0] * 0.2)
        # Split the data into training/testing sets
        self.m2_X_train = m2_X[:-test_remain,:]
        self.m2_X_test = m2_X[-test_remain:,:]
        # Split the targets into training/testing sets
        self.price_Y_train = price_Y[:-test_remain,:]
        self.price_Y_test = price_Y[-test_remain:,:]
    
    def save_dataset(self):
        ''' Saves the dataset back to the csv file '''
        self.rentals.to_csv(self.DATASET_FILE, sep=self.CSV_DELIMITER)
    
    def _load_dataset(self):
        ''' Returns the bcn rentals dataframe '''
        return pd.read_csv(self.DATASET_FILE)
    
    def _sintetize_new_data(self, quantity):
        '''Sintetizes new rentals '''
        # Define some configurations
        AVG_DEVIATION_LIMIT_PRICE = 200
        AVG_DEVIATION_LIMIT_M2 = 40
        AVG_DEVIATION_LIMIT_ROOMS = 1
        # Get the current averages
        averages = self.rentals.groupby('location').mean()
        # For each location, add N new entries
        for location in self.rentals.location.unique():
            m2_avg = averages['m2'][location]
            rooms_avg = averages['number_of_rooms'][location]
            price_avg = averages['price_per_month'][location]
            for i in range(quantity):
                # Create some new values and add them to the dataframe
                new_m2 = np.random.randint(m2_avg - AVG_DEVIATION_LIMIT_M2, m2_avg + AVG_DEVIATION_LIMIT_M2)
                new_rooms = np.random.randint(rooms_avg - AVG_DEVIATION_LIMIT_ROOMS, rooms_avg + AVG_DEVIATION_LIMIT_ROOMS)
                if new_rooms == 0:
                    new_rooms += 1
                new_price = np.random.randint(price_avg - AVG_DEVIATION_LIMIT_PRICE, price_avg + AVG_DEVIATION_LIMIT_PRICE)
                new_price += new_m2 * new_rooms
                new_row = {'m2': new_m2, 'number_of_rooms': new_rooms, 'price_per_month': new_price, 'location': location}
                self.rentals = self.rentals.append(new_row, ignore_index=True)

    def train_sample(self):
        ''' Returns the train sample '''
        return (self.m2_X_train, self.price_Y_train)
    
    def test_sample(self):
        ''' Returns the test sample '''
        return (self.m2_X_test, self.price_Y_test)
