# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.2'
#       jupytext_version: 1.2.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %%
import pickle
import os.path
import matplotlib.pyplot as plt
import numpy as np
from sklearn import linear_model
from sklearn.metrics import mean_squared_error, r2_score


# %%
class HouseRentModel(object):
    ''' House rent model with price predictions '''
    
    MODEL_FILE = 'model.pkl'
    
    def __init__(self, model):
        ''' Model constructor '''
        self.model = model
    
    def predict(self, m2):
        ''' Accepts a scalar or list of values, then returns the predicted value/s '''
        m2_type = type(m2)
        if m2_type == int or m2_type == float:
            m2_values = np.array([[m2]])
        elif m2_type == list or m2_type == tuple:
            m2_values = np.array([m2]).T
        elif m2_type == np.array:
            pass
        else:
            raise ValueError(f'Unexpected type {m2_type}, expected int/float as scalar or list/tuple of scalars')
        return self.model.predict(m2_values).astype(int)
    
    def save_model(self):
        ''' Saves the model into file '''
        module_path = os.path.dirname(os.path.abspath(__file__))
        model_file = os.path.join(module_path, self.MODEL_FILE)
        with open(model_file,'wb') as model_file:
            pickle.dump(self.model, model_file)
    
    def load_model(self):
        ''' Loads the model from file '''
        module_path = os.path.dirname(os.path.abspath(__file__))
        model_file = os.path.join(module_path, self.MODEL_FILE)
        with open(model_file,'rb') as model_file:
            self.model = pickle.load(model_file)


class HouseRentModelBuilder(object):
    ''' House rent model builder '''
    
    def __init__(self, rentals_dataset=None):
        ''' Model constructor '''
        self.model = linear_model.LinearRegression()
        self.rentals_dataset = rentals_dataset
    
    def train(self):
        ''' Use the dataset to train the model '''
        m2_X_train, price_Y_train = self.rentals_dataset.train_sample()
        self.model.fit(m2_X_train, price_Y_train)
    
    def model_info(self):
        ''' Get model info '''
        m2_X_test, price_Y_test = self.rentals_dataset.test_sample()
        price_Y_pred = self.model.predict(m2_X_test)
        model_info_data = {}
        model_info_data['mse'] = mean_squared_error(price_Y_test, price_Y_pred)
        model_info_data['r2'] = r2_score(price_Y_test, price_Y_pred)
        model_info_data['parameters'] = self.model.coef_[0]
        model_info_data['bias_parameter'] = self.model.intercept_[0]
        return model_info_data
    
    def print_model_info(self):
        ''' Print model info '''
        model_info_data = self.model_info()
        # The coefficients
        print(f'Coefficients: \n w1 = {model_info_data["parameters"]}\n b = {model_info_data["bias_parameter"]}')
        # The mean squared error
        print(f'Mean squared error: {model_info_data["mse"]:.2f}')
        # Explained variance score: 1 is perfect prediction
        print(f'Variance score: {model_info_data["r2"]:.2f}')
    
    def plot_model(self):
        ''' Plot model info '''
        m2_X_test, price_Y_test = self.rentals_dataset.test_sample()
        price_Y_pred = self.model.predict(m2_X_test)
        # Plot outputs
        plt.scatter(m2_X_test, price_Y_test,  color='black')
        plt.plot(m2_X_test, price_Y_pred, color='red', linewidth=3)
        # Remove axis
        plt.xticks(())
        plt.yticks(())
        # Show plot
        plt.show()
    
    def get_model(self):
        ''' Get a HouseRentModel '''
        return HouseRentModel(self.model)

