import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="housemodel",
    version="1.0.0",
    author="Sergio Perez",
    author_email="sergiopr89@gmail.com",
    license='MIT',
    description="House rentals model",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/ml-house-rent/housemodel",
    packages=setuptools.find_packages(),
    install_requires=[
        "numpy",
        "matplotlib",
        "scikit-learn",
        "pandas"
    ],
    package_data={"housemodel": ["model.pkl"]},
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)

